#include "constants.h"

namespace FPT {
    Constants Constants::get_constants(enum Location location) {
        Constants result;

        if (location == Location::DaNang) {
            // Link: http://reset-dn.fpt.edu.vn/FPT-Exam.zip
            // Date: 11122018
            // SHA1: 46076eb98472baa7f8a8e31b53c7c7ee0f7eb05b
            result.db_server = "10.9.9.12,1433";
            result.db_name = "ExamClient";
            result.db_username = "FPTDN";
            result.db_password = "@1123@123aA@AsynchronousPocessing=True";

            result.version = "EXAM@405@85!!7";
        } else if (location == Location::HoChiMinh) {
            // Filename: FPT-Exam-10122018.rar
            // SHA1: f8fd888d862d50375d3a40540d8a03047e5d2ce9
            result.db_server = "10.82.3.20,1433";
            result.db_name = "ExamClient";
            result.db_username = "FPTP";
            result.db_password = "#!@AAAsynchronousPocessing=True";

            result.version = "EXAM@306@89!0";
        }

        result.flags = {
            ":/flags/0.png",
            ":/flags/1.png",
            ":/flags/2.png",
            ":/flags/3.png",
            ":/flags/4.png",
            ":/flags/5.png",
            ":/flags/6.png",
            ":/flags/7.png",
            ":/flags/8.png",
            ":/flags/9.png"
        };

        return result;
    }
}
