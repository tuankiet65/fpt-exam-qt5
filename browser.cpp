#include "browser.h"

FPT::Browser::Page::Page(QWebEngineProfile *profile, QWidget *parent)
    : QWebEnginePage(profile, parent) {
    setAudioMuted(true);

#if (QT_VERSION >= QT_VERSION_CHECK(5, 11, 0))
    settings()->setUnknownUrlSchemePolicy(QWebEngineSettings::DisallowUnknownUrlSchemes);
    settings()->setAttribute(QWebEngineSettings::PlaybackRequiresUserGesture, true);
#endif

#if (QT_VERSION >= QT_VERSION_CHECK(5, 7, 0))
    settings()->setAttribute(QWebEngineSettings::Accelerated2dCanvasEnabled, true);
#endif

    settings()->setAttribute(QWebEngineSettings::XSSAuditingEnabled, true);
    settings()->setAttribute(QWebEngineSettings::JavascriptCanOpenWindows, false);
    settings()->setAttribute(QWebEngineSettings::LocalStorageEnabled, false);
}

bool FPT::Browser::Page::certificateError(const QWebEngineCertificateError &err) {
    qDebug() << "SSL error, ignoring:";
    qDebug() << err.errorDescription();
    return true;
}

FPT::Browser::View::View(QWidget *parent)
    : QWebEngineView(parent) {
    profile = new QWebEngineProfile(this);
    page = new FPT::Browser::Page(profile, this);
    this->setPage(page);
}

FPT::Browser::View::~View() {
    // Explicitly delete page before profile
    // Excerp from https://doc.qt.io/qt-5/qwebenginepage.html#QWebEnginePage-1
    // If the profile is not the default profile, the caller must ensure that the profile stays alive for as long as the page does.
    delete page;
    page = nullptr;

    delete profile;
    profile = nullptr;
}

