#include "main_window.h"

FPT::MainWindow::MainWindow(QWidget *parent) : QWidget(parent) {
    config_window();

    config_browser();

    button_exit = new FPT::Buttons::ExitButton(this);
    connect(button_exit, &FPT::Buttons::ExitButton::clicked, this, &QWidget::close);

    username = new FPT::Labels::Username(this);
    flag = new FPT::Labels::Flag(this);

    spinner = new FPT::Labels::LoadingSpinner(this);
    connect(browser, &FPT::Browser::View::loadStarted, spinner, &FPT::Labels::LoadingSpinner::show);
    connect(browser, &FPT::Browser::View::loadFinished, spinner, &FPT::Labels::LoadingSpinner::hide);

    config_layout();
}

void FPT::MainWindow::config_window() {
    // white background
    QPalette palette = this->palette();
    palette.setColor(QPalette::Background, Qt::white);
    this->setPalette(palette);

    setWindowTitle("FPT Exam");
    setMinimumSize(640, 480);

    setWindowIcon(QPixmap(":/icon.png"));
}

void FPT::MainWindow::config_browser() {
    browser_back = new FPT::Buttons::NavigationButton(QPixmap(":/browser/back.png"), this);
    browser_forward = new FPT::Buttons::NavigationButton(QPixmap(":/browser/forward.png"), this);
    browser_refresh = new FPT::Buttons::NavigationButton(QPixmap(":/browser/refresh.png"), this);

    browser = new FPT::Browser::View(this);

    connect(browser_back, &FPT::Buttons::NavigationButton::clicked,
            browser, &FPT::Browser::View::back);
    connect(browser_forward, &FPT::Buttons::NavigationButton::clicked,
            browser, &FPT::Browser::View::forward);
    connect(browser_refresh, &FPT::Buttons::NavigationButton::clicked,
            browser, &FPT::Browser::View::reload);
}

void FPT::MainWindow::config_layout() {
    QGridLayout *layout = new QGridLayout(this);

    layout->addWidget(browser_back, 0, 0, 1, 1);
    layout->addWidget(browser_forward, 0, 1, 1, 1);
    layout->addWidget(browser_refresh, 0, 2, 1, 1);
    layout->addWidget(flag, 0, 3, 2, 1);
    layout->addWidget(spinner, 0, 4, 2, 1);
    layout->addWidget(button_exit, 0, 6, 1, -1);
    
    layout->addWidget(username, 1, 0, 1, 3);

    layout->addWidget(browser, 2, 0, -1, -1);
    layout->setRowStretch(2, 1);

    layout->setMargin(0);
}

FPT::Browser::View* FPT::MainWindow::get_browser() {
    return browser;
}

FPT::Labels::Flag* FPT::MainWindow::get_flag() {
    return flag;
}

void FPT::MainWindow::closeEvent(QCloseEvent *event) {
    QMessageBox exit_msg;
    exit_msg.setText("Do you want to exit?");
    exit_msg.setStandardButtons(QMessageBox::Button::Ok | QMessageBox::Button::Cancel);
    exit_msg.setIcon(QMessageBox::Icon::Question);
    if (exit_msg.exec() == QMessageBox::Button::Ok) {
        event->accept();
    } else {
        event->ignore();
    }
}
