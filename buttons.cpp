#include "buttons.h"

FPT::Buttons::NavigationButton::NavigationButton(QPixmap icon, QWidget *parent)
    : QToolButton(parent) {
    this->setFixedSize(40, 40);
    this->setIcon(icon);
    this->setIconSize(QSize(35, 35));
    this->setAutoRaise(true);
}

FPT::Buttons::ExitButton::ExitButton(QWidget *parent)
    : QPushButton(parent) {

    QFont button_exit_font = QFont("Microsoft Sans Serif", 8, QFont::Bold);
    setText("Exit");
    setFixedSize(58, 25);
    setFont(button_exit_font);
    setStyleSheet("color: red; background-color: #e1e1e1; border: 1px solid #adadad;");
}
