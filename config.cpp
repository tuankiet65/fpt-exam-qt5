#include "config.h"

FPT::ExamConfig::ExamConfig(QString server, QString username,
                            QString password, QString db_name) {
    connection_string = QString(
        "Driver=%1;Server=%2;Uid=%3;Pwd=%4;Database=%5;"
    ).arg(DRIVER, server, username, password, db_name);

    db = QSqlDatabase::addDatabase("QODBC");
    db.setDatabaseName(connection_string);
    db.setConnectOptions("SQL_ATTR_CONNECTION_TIMEOUT=3");
}

bool FPT::ExamConfig::exec() {
    if (!db.open()) {
        qDebug() << "fail to connect";
        qDebug() << "dbtext: " << db.lastError().databaseText();
        qDebug() << "drivertext: " << db.lastError().driverText();

        success = false;
        error = QString(
            "Error while connecting to SQL sever\n"
            "databaseText: %1\n"
            "driverText: %2\n"
        ).arg(db.lastError().databaseText(), db.lastError().driverText());

        return false;
    }

    QSqlQuery query(db);
    query.setForwardOnly(true);

    query.exec("exec SP_SelectExamConfig;");
    if (query.lastError().isValid()) {
        qDebug() << "SQL Query error while executing SP_SelectExamConfig:" << query.lastError().text();

        success = false;
        error = QString(
            "Error while executing SP_SelectExamConfig\n"
            "text: %1\n"
        ).arg(query.lastError().text());

        return false;
    }
    for (int i = 1; query.next(); i++) {
        QVariant res = query.value(1);
        switch (i) {
            case 1: student_subnet.set_ipv4_byte(0, static_cast<quint8>(res.toUInt())); break;
            case 2: student_subnet.set_ipv4_byte(1, static_cast<quint8>(res.toUInt())); break;
            case 7: student_ssid = res.toString(); break;
            case 8: moodle_subnet.setAddress(res.toString()); break;
            case 9: moodle_domain = res.toString(); break;
            case 10: dns_server.setAddress(res.toString()); break;
            case 12: version = query.value(2).toString(); break; // exception
            case 13: moodle_url = res.toString(); break;
        }
    }
    query.finish();

    query.exec("exec SP_DMY;");
    if (query.lastError().isValid()) {
        qDebug() << "SQL Query error while executing SP_DMY:" << query.lastError().text();

        success = false;
        error = QString(
            "Error while executing SP_DMY\n"
            "text: %1\n"
        ).arg(query.lastError().text());

        return false;
    }
    query.next();
    day = query.value(0).toInt();
    month = query.value(1).toInt();
    year = query.value(2).toInt();
    query.finish();

    db.close();

    return true;
}
