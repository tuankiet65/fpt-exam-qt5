#pragma once

#include <QSize>
#include <QApplication>
#include <QtWebEngineWidgets>

namespace FPT {
    namespace Browser {
        class Page: public QWebEnginePage {
            Q_OBJECT
        public:
            Page(QWebEngineProfile *profile, QWidget *parent = nullptr);

            bool certificateError(const QWebEngineCertificateError &err);
        };

        class View: public QWebEngineView {
            Q_OBJECT

        private:
            QWebEngineProfile *profile;
            Page *page;

        public:
            View(QWidget *parent = nullptr);
            ~View();
        };
    }
}
