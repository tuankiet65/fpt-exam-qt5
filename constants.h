#pragma once

#include <QString>
#include <QList>

namespace FPT {
    enum Location {
        DaNang,
        HoChiMinh
    };

    class Constants {
        QString db_server;
        QString db_name;
        QString db_password;
        QString db_username;

        QString version;

        QVector<QString> flags;

    public:
        static Constants get_constants(enum Location location);

        QString get_db_server() { return db_server; }
        QString get_db_name() { return db_name; }
        QString get_db_password() { return db_password; }
        QString get_db_username() { return db_username; }

        QString get_version() { return version; }

        QVector<QString> get_flags() { return flags; } 
    };
}
