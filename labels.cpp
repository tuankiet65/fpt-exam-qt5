#include "labels.h"

FPT::Labels::Flag::Flag(QWidget *parent) : QLabel(parent) {
    this->setAlignment(Qt::AlignRight);
    this->setFixedSize(350, 79);
}

void FPT::Labels::Flag::set_available_flags(QVector<QString> flag_filenames) {
    flags.clear();

    for (auto i = flag_filenames.begin(); i != flag_filenames.end(); i++) {
        QPixmap flag(*i);
        flag = flag.scaled(QSize(170, 79), Qt::IgnoreAspectRatio, Qt::SmoothTransformation);
        flags.push_back(flag);
    }
}

bool FPT::Labels::Flag::set_flag(int flag) {
    if (!(flag >= 0 || flag < flags.size())) return false;
    this->setPixmap(flags[flag]);
    return true;
}

FPT::Labels::Username::Username(QWidget *parent): QLabel(parent) {
    this->setFixedHeight(42);
    
    this->setText("Student");
    this->setTextFormat(Qt::TextFormat::PlainText);
    
    QFont label_font = QFont("Microsoft Sans Serif", 28, QFont::Normal);
    this->setFont(label_font);
}

FPT::Labels::LoadingSpinner::LoadingSpinner(QWidget *parent): QLabel(parent) {
    spinner_gif = new QMovie(":/browser/spinner.gif");
    spinner_gif->setScaledSize(QSize(79, 79));

    this->setMovie(spinner_gif);
    spinner_gif->start();
}
