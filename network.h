#pragma once

#include <QHostAddress>

namespace FPT {
    class HostAddressNetmask: public QHostAddress {
    private:
        int netmask;

    public:
        void set_netmask(int netMask);
        bool set_ipv4_byte(quint8 byte, quint8 value);
        bool is_in_subnet(const QHostAddress &subnet);
    };
}
