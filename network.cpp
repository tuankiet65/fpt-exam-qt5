#include "network.h"

void FPT::HostAddressNetmask::set_netmask(int netmask) {
    this->netmask = netmask;
}

bool FPT::HostAddressNetmask::set_ipv4_byte(quint8 byte, quint8 value) {
    if (byte >= 4) return false;
    if (protocol() == QAbstractSocket::IPv6Protocol
        || protocol() == QAbstractSocket::UnknownNetworkLayerProtocol) {
        
        return false;
    }

    quint32 MASK = 255U << (static_cast<quint32>(byte) << 3);
    quint32 current_address = toIPv4Address(), new_address;
    new_address = (current_address & MASK); // clear byte
    new_address = (new_address | (static_cast<quint32>(value) << (static_cast<quint32>(byte) << 3))); // set byte

    setAddress(new_address);

    return true;
}

bool FPT::HostAddressNetmask::is_in_subnet(const QHostAddress &address) {
    return address.isInSubnet(*this, this->netmask);
}
