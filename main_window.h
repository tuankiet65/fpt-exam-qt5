#pragma once

#include <QApplication>
#include <QSizePolicy>
#include <QPushButton>
#include <QWidget>

#include "browser.h"
#include "labels.h"
#include "buttons.h"

namespace FPT {
    class MainWindow: public QWidget {
        Q_OBJECT

    private:
        FPT::Buttons::NavigationButton *browser_forward, *browser_back, *browser_refresh;
        FPT::Buttons::ExitButton *button_exit;

        FPT::Browser::View *browser;

        FPT::Labels::Flag *flag;
        FPT::Labels::Username *username;
        FPT::Labels::LoadingSpinner *spinner;
        
        void config_window();
        void config_browser();
        void config_layout();

    public:
        MainWindow (QWidget *parent = nullptr);

        FPT::Browser::View* get_browser();
        FPT::Labels::Flag* get_flag();

        void closeEvent(QCloseEvent *event);
    };
}
