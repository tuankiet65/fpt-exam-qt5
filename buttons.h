#pragma once

#include <QToolButton>
#include <QPushButton>

namespace FPT {
    namespace Buttons {
        class NavigationButton: public QToolButton {
            Q_OBJECT

        public:
            NavigationButton(QPixmap icon, QWidget *parent = nullptr);
        };

        class ExitButton: public QPushButton {
            Q_OBJECT
        
        public:
            ExitButton(QWidget *parent = nullptr);
        };
    }
}
