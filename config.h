#pragma once

#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlError>

#include <QString>

#include <QUrl>

#include "network.h"

namespace FPT {
    class ExamConfig {
    private:
#if defined(Q_OS_WIN)
        const QString DRIVER = "{SQL Server}";
#elif defined(Q_OS_LINUX) || defined(Q_OS_UNIX)
        const QString DRIVER = "libtdsodbc.so";
#endif
        bool success = true;
        QString connection_string = "";
        QString error = "";

        HostAddressNetmask student_subnet;
        QString student_ssid = "";

        HostAddressNetmask dns_server;

        HostAddressNetmask moodle_subnet;
        QString moodle_domain = "";
        QUrl moodle_url = QUrl("https://duckduckgo.com");

        QString version = "";

        int day = 0, month = 0, year = 0;

        QSqlDatabase db;

    public:
        ExamConfig(QString db_url, QString db_username, QString db_password, QString db_name);
        
        bool exec();

        bool get_success() { return success; }
        QString get_connection_string() { return connection_string; }
        QString get_error() { return error; }
        HostAddressNetmask get_student_subnet() { return student_subnet; }
        QString get_student_ssid() { return student_ssid; }
        HostAddressNetmask get_dns_server() { return dns_server; }
        HostAddressNetmask get_moodle_subnet() { return moodle_subnet; }
        QString get_moodle_domain() { return moodle_domain; }
        QUrl get_moodle_url() { return moodle_url; }
        QString get_version() { return version; }

        int get_day() { return day; }
        int get_month() { return month; }
        int get_year() { return year; }
    };
}
