#include <QApplication>
#include <QSplashScreen>
#include <QPixmap>
#include <QMessageBox>
#include <QFontDatabase>

#include "config.h"
#include "constants.h"
#include "main_window.h"

FPT::Constants constants;

int main(int argc, char *argv[]) {
    QApplication app(argc, argv);

    QFontDatabase::addApplicationFont(":/micross.ttf");
    
    QSplashScreen splash(QPixmap(":/splash.png"));
    splash.show();
    splash.showMessage("Loading exam configurations from exam server...", Qt::AlignRight);

    QMessageBox location_select;
    location_select.setText("Select your location:");
    location_select.setIcon(QMessageBox::Icon::Question);
    QAbstractButton *hcm_button = location_select.addButton("Ho Chi Minh", QMessageBox::ButtonRole::ActionRole);
    QAbstractButton *dn_button = location_select.addButton("Da Nang", QMessageBox::ButtonRole::ActionRole);
    location_select.exec();
    if (location_select.clickedButton() == hcm_button) {
        constants = FPT::Constants::get_constants(FPT::Location::HoChiMinh);
    } else if (location_select.clickedButton() == dn_button) {
        constants = FPT::Constants::get_constants(FPT::Location::DaNang);
    } else if (location_select.clickedButton() == nullptr) {
        return 0;
    }

    FPT::ExamConfig config(constants.get_db_server(),
                           constants.get_db_username(),
                           constants.get_db_password(),
                           constants.get_db_name());
    
    if (!config.exec()) {
        QMessageBox error_msg;
        error_msg.setText("Error connecting to the database");
        error_msg.setInformativeText(
            "FPT Exam is unable to connect to the exam server. "
            "Do you want to continue anyway? "
            "(notice: you'll be unable to access the exam site)"
        );
        error_msg.setDetailedText(
            QString(
                "Connection string: '%1'\n"
                "error: \n%2"
            ).arg(
                config.get_connection_string(),
                config.get_error()
            )
        );
        error_msg.setStandardButtons(QMessageBox::Button::Ok | QMessageBox::Button::Cancel);
        error_msg.setIcon(QMessageBox::Icon::Critical);
        if (error_msg.exec() == QMessageBox::Button::Cancel) {
            return 0;
        }
    }

    splash.showMessage("Checking software version...", Qt::AlignRight);
    if (config.get_version() != constants.get_version()) {
        QMessageBox version_msg;
        version_msg.setText("Version mismatched");
        version_msg.setInformativeText(
            "The current software version does not match exam server's required one. "
            "You are advised to update the software. "
            "Do you still want to continue? "
            "(you might not be able to take exams if you continue)"
        );
        version_msg.setDetailedText(
            QString(
                "Current version is '%1', but exam server requires version '%2'."
            ).arg(constants.get_version(), config.get_version())
        );
        version_msg.setStandardButtons(QMessageBox::Button::Ok | QMessageBox::Button::Cancel);
        version_msg.setIcon(QMessageBox::Icon::Warning);
        if (version_msg.exec() == QMessageBox::Button::Cancel) {
            return 0;
        }
    }

    splash.showMessage("Loading exam software...", Qt::AlignRight);

    FPT::MainWindow window;
    window.setWindowTitle("FPT Exam");
    window.get_browser()->load(config.get_moodle_url());
    window.get_flag()->set_available_flags(constants.get_flags());
    window.get_flag()->set_flag((config.get_day() + config.get_month()) % 10);
    window.setMinimumSize(640, 480);
    window.showFullScreen();
    
    splash.finish(&window);

    return app.exec();
}
