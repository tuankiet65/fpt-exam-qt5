#pragma once

#include <QLabel>
#include <QMovie>

namespace FPT {
    namespace Labels {
        class Flag: public QLabel {
            Q_OBJECT

        private:
            QList<QPixmap> flags;

        public:
            Flag(QWidget *parent = nullptr);
            void set_available_flags(QVector<QString> flag_filenames);

        public slots:
            bool set_flag(int flag_num);
        };

        class Username: public QLabel {
            Q_OBJECT

        public:
            Username(QWidget *parent = nullptr);
        };

        class LoadingSpinner: public QLabel {
            Q_OBJECT
            
        private:
            QMovie *spinner_gif;
        public:
            LoadingSpinner(QWidget *parent = nullptr);
        };
    }
}
